class CreateChars < ActiveRecord::Migration
  def change
    create_table :chars do |t|
      t.string :title
      t.integer :number

      t.timestamps null: false
    end
  end
end
