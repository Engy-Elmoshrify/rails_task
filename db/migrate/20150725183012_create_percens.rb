class CreatePercens < ActiveRecord::Migration
  def change
    create_table :percens do |t|
      t.integer :chartid
      t.string :name
      t.float :per

      t.timestamps null: false
    end
  end
end
